package com.jenkins.demo.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/jenkins")
public class TestController {

    @GetMapping()
    public ResponseEntity<?> testJenkins(){
        String hello = "Hello Rupesh !!! Some Changes Done.";
        return ResponseEntity.ok(hello);
    }
}
